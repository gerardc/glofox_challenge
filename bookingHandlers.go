package main

import (
	"net/http"
	"time"

	"github.com/gin-gonic/gin"
)

type Booking struct {
	Name  string     `json:"name,omitempty"`
	Class string     `json:"class,omitempty"`
	Date  *time.Time `json:"date,omitempty"`
}

var bookingDB = make(map[string]Booking)

func createBooking(c *gin.Context) {
	var booking Booking
	if err := c.ShouldBindJSON(&booking); err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
		return
	}

	bookingDB[booking.Name] = booking
	c.JSON(http.StatusOK, bookingDB[booking.Name])
}

func getBookings(c *gin.Context) {
	values := []Booking{}
	for _, v := range bookingDB {
		values = append(values, v)
	}
	c.JSON(http.StatusOK, values)
}

func getBooking(c *gin.Context) {
	name := c.Param("name")
	value, exists := bookingDB[name]
	if exists {
		c.JSON(http.StatusOK, value)
	} else {
		c.JSON(http.StatusNotFound, Booking{})
	}
}

func patchBooking(c *gin.Context) {
	var inputBooking Booking
	if err := c.ShouldBindJSON(&inputBooking); err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
		return
	}

	name := c.Param("name")
	booking := bookingDB[name]

	// Assuming we should only modify when non-empty value is present
	if inputBooking.Name != "" {
		booking.Name = inputBooking.Name
		// When we change the name we should remove the old entry in the map
		delete(bookingDB, name)
	}
	if inputBooking.Date != nil {
		booking.Date = inputBooking.Date
	}
	if inputBooking.Class != "" {
		booking.Class = inputBooking.Class
	}

	bookingDB[booking.Name] = booking
	c.JSON(http.StatusOK, bookingDB[booking.Name])
}

func updateBooking(c *gin.Context) {
	var inputBooking Booking
	if err := c.ShouldBindJSON(&inputBooking); err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
		return
	}

	name := c.Param("name")
	delete(bookingDB, name)
	bookingDB[inputBooking.Name] = inputBooking

	c.JSON(http.StatusOK, bookingDB[inputBooking.Name])
}

func deleteBooking(c *gin.Context) {
	name := c.Param("name")
	booking := bookingDB[name]
	delete(bookingDB, name)
	c.JSON(http.StatusOK, booking)
}
