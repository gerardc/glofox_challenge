package main

import (
	"net/http"
	"time"

	"github.com/gin-gonic/gin"
)

type Class struct {
	Name      string     `json:"name,omitempty"`
	StartDate *time.Time `json:"start_date,omitempty"`
	EndDate   *time.Time `json:"end_date,omitempty"`
	Capacity  int        `json:"capacity,omitempty"`
}

var classDB = make(map[string]Class)

func createClass(c *gin.Context) {
	var class Class
	if err := c.ShouldBindJSON(&class); err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
		return
	}

	classDB[class.Name] = class
	c.JSON(http.StatusOK, classDB[class.Name])
}

func getClass(c *gin.Context) {
	name := c.Param("name")
	value, exists := classDB[name]
	if exists {
		c.JSON(http.StatusOK, value)
	} else {
		c.JSON(http.StatusNotFound, Class{})
	}
}

func deleteClass(c *gin.Context) {
	name := c.Param("name")
	class := classDB[name]
	delete(classDB, name)
	c.JSON(http.StatusOK, class)
}

func getClasses(c *gin.Context) {
	values := []Class{}
	for _, v := range classDB {
		values = append(values, v)
	}
	c.JSON(http.StatusOK, values)
}

func patchClass(c *gin.Context) {
	var inputClass Class
	if err := c.ShouldBindJSON(&inputClass); err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
		return
	}

	name := c.Param("name")
	class := classDB[name]

	// Assuming we should only modify when non-empty value is present
	if inputClass.Name != "" {
		class.Name = inputClass.Name
		// When we change the name we should remove the old entry in the map
		delete(classDB, name)
	}
	if inputClass.StartDate != nil {
		class.StartDate = inputClass.StartDate
	}
	if inputClass.EndDate != nil {
		class.EndDate = inputClass.EndDate
	}
	if inputClass.Capacity != 0 {
		class.Capacity = inputClass.Capacity
	}

	classDB[class.Name] = class
	c.JSON(http.StatusOK, classDB[class.Name])
}

func updateClass(c *gin.Context) {
	var inputClass Class
	if err := c.ShouldBindJSON(&inputClass); err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
		return
	}

	name := c.Param("name")
	delete(classDB, name)
	classDB[inputClass.Name] = inputClass

	c.JSON(http.StatusOK, classDB[inputClass.Name])
}
