# Glofox Challenge

- I've implemented the solution in Go using the Gin web service library
- `main.go` is the entry point with endpoint handlers residing in the
  `classHandlers.go` and `bookingHandlers.go` files.
- `main_test.go` contains integration-style tests for each sets of endpoints.

### Running locally

- Install go with `brew install go` (assuming mac os)
- [Set up a go workspace](https://golang.org/doc/code.html#Workspaces)
- Run `go get "github.com/stretchr/testify/assert"` (installs a test dependency)
- Run `go get "github.com/gin-gonic/gin"` (installs the Gin library) 
- Run service with `go run main.go ./classHandlers.go ./bookingHandlers.go`
- Run tests with `go test ./`

## Curl commands for testing:

### CRUD for a class:
```
CREATE
curl localhost:8080/classes -d'{"name":"pilates","start_date":"2018-01-01T00:00:00Z","end_date":"2018-01-01T00:00:00Z","capacity":20}'

READ
curl localhost:8080/classes/pilates

LIST
curl localhost:8080/classes

UPDATE (with PATCH -> partial update)
curl -XPATCH localhost:8080/classes/pilates -d'{"capacity":10}'

UPDATE (with PUT -> replace with...)
curl -XPUT localhost:8080/classes/pilates -d'{"name":"yoga","start_date":"2019-01-01T00:00:00Z","end_date":"2019-01-01T00:00:00Z","capacity":20}'

DELETE
curl -XDELETE localhost:8080/classes/yoga
```

### CRUD for a booking
```
CREATE
curl localhost:8080/bookings -d'{"name":"john","class":"pilates","date":"2018-01-01T00:00:00Z"}'

READ
curl localhost:8080/bookings/john

LIST
curl localhost:8080/bookings

UPDATE (with PATCH -> partial update)
curl -XPATCH localhost:8080/bookings/john -d'{"class":"yoga"}'

UPDATE (with PUT -> replace with...)
curl -XPUT localhost:8080/bookings/john -d'{"name":"marie","class":"pilates","date":"2019-01-01T00:00:00Z"}'

DELETE
curl -XDELETE localhost:8080/bookings/marie
```