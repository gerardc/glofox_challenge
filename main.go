package main

import (
	"net/http"

	"github.com/gin-gonic/gin"
)

func setupRouter() *gin.Engine {
	r := gin.Default()

	// Ping test
	r.GET("/ping", func(c *gin.Context) {
		c.String(http.StatusOK, "pong")
	})

	// Classes REST API
	r.GET("/classes", getClasses)
	r.POST("/classes", createClass)
	r.GET("/classes/:name", getClass)
	r.PATCH("/classes/:name", patchClass)
	r.PUT("/classes/:name", updateClass)
	r.DELETE("/classes/:name", deleteClass)

	// Bookings REST API
	r.GET("/bookings", getBookings)
	r.POST("/bookings", createBooking)
	r.GET("/bookings/:name", getBooking)
	r.PATCH("/bookings/:name", patchBooking)
	r.PUT("/bookings/:name", updateBooking)
	r.DELETE("/bookings/:name", deleteBooking)

	return r
}

func main() {
	r := setupRouter()
	r.Run(":8080")
}
