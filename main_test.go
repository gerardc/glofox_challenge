package main

import (
	"fmt"
	"net/http"
	"net/http/httptest"
	"strings"
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestPingRoute(t *testing.T) {
	router := setupRouter()

	w := httptest.NewRecorder()
	req, _ := http.NewRequest("GET", "/ping", nil)
	router.ServeHTTP(w, req)

	assert.Equal(t, http.StatusOK, w.Code)
	assert.Equal(t, "pong", w.Body.String())
}

func TestRESTRoutesForClass(t *testing.T) {
	router := setupRouter()

	body := `{"name":"pilates","start_date":"2018-01-01T00:00:00Z","end_date":"2018-01-01T00:00:00Z","capacity":20}`

	// POST Class
	w := httptest.NewRecorder()
	req, _ := http.NewRequest("POST", "/classes", strings.NewReader(body))
	router.ServeHTTP(w, req)

	assert.Equal(t, http.StatusOK, w.Code)
	assert.Equal(t, fmt.Sprintf("%s\n", body), w.Body.String())

	// GET a class
	w = httptest.NewRecorder()
	req, _ = http.NewRequest("GET", "/classes/pilates", nil)
	router.ServeHTTP(w, req)

	assert.Equal(t, http.StatusOK, w.Code)
	assert.Equal(t, fmt.Sprintf("%s\n", body), w.Body.String())

	// GET all classes
	w = httptest.NewRecorder()
	req, _ = http.NewRequest("GET", "/classes", nil)
	router.ServeHTTP(w, req)

	assert.Equal(t, http.StatusOK, w.Code)
	assert.Equal(t, fmt.Sprintf("[%s]\n", body), w.Body.String())

	// PATCH a class
	body = `{"name":"yoga","start_date":"2018-01-01T00:00:00Z","end_date":"2018-01-01T00:00:00Z","capacity":20}`

	w = httptest.NewRecorder()
	req, _ = http.NewRequest("PATCH", "/classes/pilates", strings.NewReader(`{"name":"yoga"}`))
	router.ServeHTTP(w, req)

	assert.Equal(t, http.StatusOK, w.Code)
	assert.Equal(t, fmt.Sprintf("%s\n", body), w.Body.String())

	// PUT a class
	body = `{"name":"boxing","start_date":"2018-01-01T00:00:00Z","end_date":"2018-01-01T00:00:00Z","capacity":15}`

	w = httptest.NewRecorder()
	req, _ = http.NewRequest("PUT", "/classes/yoga", strings.NewReader(`{"name":"boxing","start_date":"2018-01-01T00:00:00Z","end_date":"2018-01-01T00:00:00Z","capacity":15}`))
	router.ServeHTTP(w, req)

	assert.Equal(t, http.StatusOK, w.Code)
	assert.Equal(t, fmt.Sprintf("%s\n", body), w.Body.String())

	// DELETE a class
	w = httptest.NewRecorder()
	req, _ = http.NewRequest("DELETE", "/classes/boxing", nil)
	router.ServeHTTP(w, req)

	assert.Equal(t, http.StatusOK, w.Code)
	assert.Equal(t, fmt.Sprintf("%s\n", body), w.Body.String())

	// GET all classes (should now return empty)
	w = httptest.NewRecorder()
	req, _ = http.NewRequest("GET", "/classes", nil)
	router.ServeHTTP(w, req)

	assert.Equal(t, http.StatusOK, w.Code)
	assert.Equal(t, fmt.Sprintf("[]\n"), w.Body.String())

	// GET a class (should return 404)
	w = httptest.NewRecorder()
	req, _ = http.NewRequest("GET", "/classes/pilates", nil)
	router.ServeHTTP(w, req)

	assert.Equal(t, http.StatusNotFound, w.Code)
	assert.Equal(t, "{}\n", w.Body.String())
}

func TestRESTRoutesForBooking(t *testing.T) {
	router := setupRouter()

	body := `{"name":"john","class":"pilates","date":"2018-01-01T00:00:00Z"}`

	// POST to Bookings
	w := httptest.NewRecorder()
	req, _ := http.NewRequest("POST", "/bookings", strings.NewReader(body))
	router.ServeHTTP(w, req)

	assert.Equal(t, http.StatusOK, w.Code)
	assert.Equal(t, fmt.Sprintf("%s\n", body), w.Body.String())

	// GET a booking
	w = httptest.NewRecorder()
	req, _ = http.NewRequest("GET", "/bookings/john", nil)
	router.ServeHTTP(w, req)

	assert.Equal(t, http.StatusOK, w.Code)
	assert.Equal(t, fmt.Sprintf("%s\n", body), w.Body.String())

	// GET all bookings
	w = httptest.NewRecorder()
	req, _ = http.NewRequest("GET", "/bookings", nil)
	router.ServeHTTP(w, req)

	assert.Equal(t, http.StatusOK, w.Code)
	assert.Equal(t, fmt.Sprintf("[%s]\n", body), w.Body.String())

	// PATCH a booking
	body = `{"name":"ada","class":"pilates","date":"2018-01-01T00:00:00Z"}`

	w = httptest.NewRecorder()
	req, _ = http.NewRequest("PATCH", "/bookings/john", strings.NewReader(`{"name":"ada"}`))
	router.ServeHTTP(w, req)

	assert.Equal(t, http.StatusOK, w.Code)
	assert.Equal(t, fmt.Sprintf("%s\n", body), w.Body.String())

	// PUT a booking
	body = `{"name":"marie","class":"pilates","date":"2019-01-01T00:00:00Z"}`

	w = httptest.NewRecorder()
	req, _ = http.NewRequest("PUT", "/bookings/ada", strings.NewReader(`{"name":"marie","class":"pilates","date":"2019-01-01T00:00:00Z"}`))
	router.ServeHTTP(w, req)

	assert.Equal(t, http.StatusOK, w.Code)
	assert.Equal(t, fmt.Sprintf("%s\n", body), w.Body.String())

	// DELETE a booking
	w = httptest.NewRecorder()
	req, _ = http.NewRequest("DELETE", "/bookings/marie", nil)
	router.ServeHTTP(w, req)

	assert.Equal(t, http.StatusOK, w.Code)
	assert.Equal(t, fmt.Sprintf("%s\n", body), w.Body.String())

	// GET all bookings (should now return empty)
	w = httptest.NewRecorder()
	req, _ = http.NewRequest("GET", "/bookings", nil)
	router.ServeHTTP(w, req)

	assert.Equal(t, http.StatusOK, w.Code)
	assert.Equal(t, fmt.Sprintf("[]\n"), w.Body.String())

	// GET all bookings (should return 404)
	w = httptest.NewRecorder()
	req, _ = http.NewRequest("GET", "/bookings/john", nil)
	router.ServeHTTP(w, req)

	assert.Equal(t, http.StatusNotFound, w.Code)
	assert.Equal(t, "{}\n", w.Body.String())
}
